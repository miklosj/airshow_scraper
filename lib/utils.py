months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct",
    "Nov", "Dec"]

def add_apostrophes(stringy):
    if isinstance(stringy, str):
        stringy = "\"" + stringy + "\""

    return stringy

def parse_date(yearStr, dateStr):
    dateStr = dateStr.split("/")

    dateCodes = [];
    for subStr in dateStr:
        subStr = subStr.split(" ")
        dateDay = subStr[0]
        dateDay = dateDay.replace(" ", "")
        dateDay = dateDay.split("-")
        dateMonth = subStr[1]
        dateMonth = dateMonth.replace(" "," ")
        dateMonthNum = 0
        dateMonthStr = ""

        for month in months:
            if dateMonth == month:
                dateMonthNum = months.index(month) + 1

                dateMonthStr = str(dateMonthNum)
                if len(dateMonthStr) < 2:
                    dateMonthStr = "0" + dateMonthStr

        if len(dateDay) > 1:
            for dateDayNum in dateDay:
                dateCode = dateMonthStr + "/" + dateDayNum + "/" + str(yearStr)
                dateCodes.append(dateCode)
        else:
            dateDayNum = dateDay[0]
            dateCode = dateMonthStr + "/" + dateDayNum + "/" + str(yearStr)
            dateCodes.append(dateCode)

    if len(dateCodes) == 1:
        dateCodes.append(dateCodes[0])

    return dateCodes
