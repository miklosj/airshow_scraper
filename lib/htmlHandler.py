from requests import get
from requests.exceptions import RequestException
from contextlib import closing

def html_get(url):

    try:
        with closing(get(url, stream=True)) as resp:
            if html_validate(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        error_log('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def html_validate(resp):

    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def error_log(e):

    print(e)
