def generate_empty_obj():

    obj = {
    "publish_status": "publish",
    "event_id": "",
    "color": "206177",
    "event_name": "",
    "event_description": "",
    "event_start_date": "",
    "event_start_time": "",
    "event_end_date": "",
    "event_end_time": "",
    "all_day": "",
    "hide_end_time": "",
    "event_gmap": "",
    "yearlong": "",
    "featured": "",
    "evo_location_id": "",
    "location_name": "",
    "event_location": "",
    "evo_organizer_id": "",
    "event_organizer": "",
    "evcal_subtitle": "",
    "learnmore link": "",
    "image_url": "",
    "repeatevent": "",
    "frequency": "",
    "repeats": "",
    "repeatby": "",
    "event_type": "",
    "event_type_slug": "",
    "event_type_2": "",
    "event_type_2_slug": "",
    "cmd_1": ""
    }

    return obj
