event_ids = []
location_ids = []

def generate_loc_id(locStr):
    if len(location_ids) < 1:
        id = 1000
        location_entry = {"loc" : locStr, "id" : id}
        location_ids.append(location_entry)
        return id
    else:
        max = 0

        for location_entry_read in location_ids:
            if location_entry_read["loc"] == locStr:
                return location_entry_read["id"]


        for location_entry_read in location_ids:
            if location_entry_read["id"] > max:
                max = location_entry_read["id"]

        id = max + 1
        location_entry = {"loc" : locStr, "id" : id}
        location_ids.append(location_entry)
        return id
