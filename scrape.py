from bs4 import BeautifulSoup

import csv
import os

from lib import evo_location_id_ctr
from lib import htmlHandler
from lib import columns
from lib import utils
from lib import config

if not os.path.exists("output/"):
    os.makedirs("output/")

for target in config.targets:

    try:
        print("Processing target " + target["filename"] + ".")

        raw_html = htmlHandler.html_get(target["url"])

        html = BeautifulSoup(raw_html, 'html.parser')

        table_ids = ("January_list", "February_list", "March_list", "April_list", \
            "May_list", "June_list", "July_list", "August_list", "September_list", \
            "October_list", "November_list", "December_list");

        objs = []

        year = html.find("h1").string[-4:]

        for month in table_ids:
            div = html.find("div", {"id" : month})
            if div is not None:
                all_rows = div.find_all("tr")
                info_rows = []

                for row in all_rows:
                    if row.find("td", class_="af2") is not None:
                        info_rows.append(row)

                for row in info_rows:

                    obj = columns.generate_empty_obj()

                    cells = row.contents

                    dates = utils.parse_date(year, cells[0].string);

                    obj["event_name"] = cells[1].string

                    obj["event_start_date"] = dates[0]
                    obj["event_start_time"] = "08:00:AM"
                    obj["event_end_date"] = dates[1]
                    obj["event_end_time"] = "07_00:PM"
                    obj["all_day"] = "yes"
                    obj["hide_end_time"] = "no"
                    try:
                        obj["event_gmap"] = utils.add_apostrophes(cells[2].contents[0].get("href"))
                    except Exception as e:
                        print("\"" + cells[1].contents[0].string +
                             "\" entry has no associated gmaps link.")
                    obj["yearlong"] = "no"
                    obj["featured"] = "no"
                    locStr = "";
                    for content in cells[2].contents:
                        locStr += content.string

                    obj["location_name"] = utils.add_apostrophes(locStr)
                    obj["event_location"] = utils.add_apostrophes(locStr)
                    countryStr = "";

                    try:
                        countryStr = cells[2].contents[2].string
                    except Exception as e:
                        countryStr = cells[2].contents[1].string

                    obj["evo_location_id"] = str(evo_location_id_ctr.generate_loc_id(countryStr))
                    obj["repeatevent"] = "no"
                    obj["frequency"] = "daily"
                    obj["repeats"] = "1"
                    obj["repeatby"] = "dom"
                    obj["event_type"] =	target["continent_id"]
                    obj["event_type_slug"] = target["continent"]
                    obj["event_type_2"] = "1111"
                    obj["event_type_2_slug"] = utils.add_apostrophes(countryStr);

                    objs.append(obj)

        with open("output/" + target["filename"] , "w", encoding="utf-8", newline="") as f:
            #w.writeheader()

            line = "";
            for key, value in objs[0].items():
                line += str(key) + ","
            f.write(line + "\n")

            for obj in objs:
                line = "";
                for key, value in obj.items():
                    line += str(value) + ","

                f.write(line + "\n")
            f.close()
        print("Done!")
    except Exception as e:
        print("Http connection or filesystem error!")
        print(e.stack)
